<?php
require_once __DIR__ . '/vendor/autoload.php';

const URL_NOMINATIM = "https://nominatim.openstreetmap.org/search.php?format=json&q=";

function getData($url) {
    $client = new GuzzleHttp\Client();
    $response = $client->get($url);
    $body = json_decode($response->getBody());

    return !empty($body) ? $body[0] : [];
}

use League\Csv\Reader;
use League\Csv\Writer;

if(isset($_POST["submit"])) {

    if (isset($_FILES["csvFile"]) && $_FILES["csvFile"]['type'] == "text/csv") {

        $headerNew = ['adresse', 'latitude', 'longitude'];
        $recordsNew = [];

        $csvFilePath = $_FILES["csvFile"]["tmp_name"];
        $csv = Reader::createFromPath($csvFilePath, 'r');
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);

        $header = $csv->getHeader();
        $records = $csv->getRecords();

        foreach ($records as $record) {
            $adresse = $record['adresse'];
            $url = URL_NOMINATIM . "$adresse";
            $data = getData($url);
            $lat = !empty($data->lat) ? $data->lat : "";
            $lon = !empty($data->lon) ? $data->lon : "";
            $recordsNew[] = [$adresse, $lat, $lon];
            sleep (1);
        }

        $writer = Writer::createFromString('');
        $writer->insertOne($headerNew);
        $writer->insertAll($recordsNew);

        echo $writer->getContent();

        header("Content-disposition: attachment; filename=adresses_latitude_longitude.csv");
        header('Content-Type: application/csv; charset=UTF-8');
        die;
    }
}

header('Location: index.php');